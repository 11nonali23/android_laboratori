package com.example.fragments_trial

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


//FUTURE EXERCIES:
/*
When you switch fragment you have to:

    push the undestand how to save the fragment state and restore it
*/
class MainActivity : AppCompatActivity() {

    val fragmentOneTag = "F1"
    val fragmentTwoTag = "F2"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Getting the screen tag string
        val tag = main_layout.tag.toString()

        //Disabling switch fragment button if screen is big.
        when (tag)
        {
            resources.getString(R.string.bigger_screen) ->
            {
                button_1.isEnabled = false
                button_2.isEnabled = false
            }

            resources.getString(R.string.screen_standard) ->
            {
                //Creating fragment and adding it into fragment container
                supportFragmentManager.beginTransaction().add(R.id.fragment_container, FragmentOne()).commit()
            }

            else -> Log.e("Main Activity","Error no match found for => $tag")
        }
    }

    //TO IMPROVE!!! can you help me find fragment on back stack???
    fun selectFragment (view: View) {

        when (view)
        {
            button_2 ->
            {
                //Creating fragment and adding it into fragment container and adding into backstack
                supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container, FragmentTwo())
                    .addToBackStack(fragmentTwoTag)
                    .commit()

            }

            button_1 ->
            {
                //Creating fragment and adding it into fragment container
                supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container, FragmentOne())
                    .addToBackStack(fragmentOneTag)
                    .commit()

            }
        }

        //getting number of fragment from backstack and logging them
        val count = supportFragmentManager.backStackEntryCount
        Log.e("Main Activity counter","Number of entries on backstack => $count")

    }

    //This method will get last fragment from the backstack
    fun getFragmentFromBackStack() {
        val index = supportFragmentManager.backStackEntryCount - 1
        val backEntry = supportFragmentManager.getBackStackEntryAt(index);
        val tag = backEntry.getName();
        val fragment = supportFragmentManager.findFragmentByTag(tag);
    }
}
