package com.example.todo_app

import java.text.SimpleDateFormat
import java.util.*

//Class to model a to do item

class ToDoItem {
    var toDo: String
    var date: GregorianCalendar

    constructor(toDo: String)
    {
        this.toDo = toDo
        date = GregorianCalendar()
    }

    override fun toString(): String
    {
        var currentDate = SimpleDateFormat("dd/MM/yyyy",
            Locale.ITALIAN).format(date.getTime());

        return "$currentDate  => $toDo"
    }
}