package com.example.todo_app

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.SparseBooleanArray
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


//helper article => https://medium.com/@tanunprabhu95/to-do-list-application-using-kotlin-using-android-studio-546e74ac75aa

class MainActivity : AppCompatActivity() {
    lateinit var toDoItems: ArrayList<ToDoItem>
    lateinit var adapter: ArrayAdapter<ToDoItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initializing list that contains element and adapter for listview
        toDoItems = ArrayList()
        adapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1, toDoItems)
        //adding the adapter to the listview
        list_item.adapter = adapter

    }

    //When user add an item it will go the ListView or Toast error if empty
    fun addItem (view: View)
    {
        val ToDo = ToDo_view.text.toString()
        if(ToDo.length == 0)
        {
            Toast.makeText(applicationContext, "Empty ToDo value", Toast.LENGTH_LONG).show()
            return
        }

        var toDoItem = ToDoItem(ToDo)
        toDoItems.add(toDoItem)
        adapter.notifyDataSetChanged()

        //clear text field
        ToDo_view.setText("")

        //Hide the soft key
        var inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(ToDo_view.applicationWindowToken, 0)
    }

    //ListView is multiple selecion. It will delete all select item by user
    fun delItem(view: View)
    {
       val position: SparseBooleanArray = list_item.checkedItemPositions
        val count = list_item.count

        var item = count - 1

        while (item >= 0)
        {
            if(position.get(item))
                adapter.remove(toDoItems.get(item))

            item--
        }
        position.clear()
        adapter.notifyDataSetChanged()
    }

    //It will eliminate all elements
    fun empty(view: View)
    {
        toDoItems.clear()
        adapter.notifyDataSetChanged()
    }
}

