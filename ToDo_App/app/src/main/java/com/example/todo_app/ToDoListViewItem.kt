package com.example.todo_app

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RequiresApi

//Per inserirla in un layout dovrò definire un elemento XML in un file di layout

@RequiresApi(Build.VERSION_CODES.M)
class ToDoListViewItem(context: Context, attributeSet: AttributeSet) : View(context, attributeSet)
{

    @RequiresApi(Build.VERSION_CODES.M)
    private  val paperColor = context.getColor(R.color.notepad_paper)
    private val linePaint = Paint()


    init
    {
        linePaint.setColor(context.getColor(R.color.notepad_lines))
        linePaint.strokeWidth = 4F
    }

    override fun onDraw(canvas: Canvas?)
    {
        canvas?.drawColor(paperColor)

        //draw a line after the first text line.
        val ruleLineY: Float = baseline + baseline* 0.1F //baseline = spessore del font
        val ruleLineXStart: Float = paddingLeft as Float
        val ruleLineXstop: Float = (measuredWidth - paddingRight) as Float

        canvas?.drawLine(ruleLineXStart, ruleLineY, ruleLineXstop, ruleLineY, linePaint)

        super.onDraw(canvas)
    }
}