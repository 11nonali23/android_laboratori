package com.example.es03_prof

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import java.lang.Integer.min

class CircularProgressBar(context: Context, attributeSet: AttributeSet) : View(context, attributeSet)
{

    private val rectF = RectF(0F, 0F, 0F, 0F) //mantain information on the drawing area
    private val paint = Paint()
    var percentage = 0.75F
        //setter e getter for percentage. When i set new value i have to repaint the view
        get() = field
        set(value)
        {
            field = value
            invalidate() //
            requestLayout()
        }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onDraw(canvas: Canvas?)
    {
        super.onDraw(canvas)

        paint.style = Paint.Style.STROKE //stroke = tratto (contorno). Fill riempie tutto
        paint.isAntiAlias = true

        val maxSize = min(width, height)
        paint.strokeWidth = maxSize * 0.25F
        val pad = paint.strokeWidth * 0.6F //padding leave space from the drawing area on the rectF containing it
        rectF.set(0F + pad, 0F + pad, width.toFloat() - pad, height.toFloat() - pad)

        //painting the grey arc over all the circle
        paint.color = Color.LTGRAY
        canvas?.drawArc(rectF, 0F, 360F, false, paint)

        //painting the accent arc based on the percenatge given
        var startAngle = 0F
        var drawTo = startAngle + percentage * 360
        paint.color = ContextCompat.getColor(context, R.color.colorAccent)
        canvas?.drawArc(rectF, startAngle, drawTo, false, paint)
    }
}
