package com.example.es03_prof

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat

class SemaphoreView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet)
{
    //Mi sono permesso di definire un enum al posto che utlizzare una string per evitare stringhe errate
    enum class Lights { GREEN, YELLOW, RED}

    var lightOn = Lights.GREEN
        //setter e getter for light on. When i set new value i have to repaint the view
        get() = field
        set(value)
        {
            field = value
            invalidate() //
            requestLayout()
        }

    private val rectF = RectF(0F, 0F, 0F, 0F) //mantain information on the drawing area
    private val paint = Paint()

    //Colors
    val backgroundColor = ResourcesCompat.getColor(resources, R.color.colorBackgroudSemaphore, null)
    val lightOffSemaphoreColor = ResourcesCompat.getColor(resources, R.color.lightOffSemaphoreColor, null)
    val semaphoreRed = ResourcesCompat.getColor(resources, R.color.semaphoreRed, null)
    val semaphoreYellow = ResourcesCompat.getColor(resources, R.color.semaphoreYellow, null)
    val semaphoreGreen = ResourcesCompat.getColor(resources, R.color.semaphoreGreen, null)
    val borderColor = ResourcesCompat.getColor(resources, R.color.borderColor, null)

    companion object
    {
        val strokeWidth = 4F
    }

    override fun onDraw(canvas: Canvas?)
    {

        super.onDraw(canvas)

        //Drawing light yellow rectangle
        paint.style = Paint.Style.FILL
        paint.color = backgroundColor
        rectF.set(0F , 0F , width.toFloat() , height.toFloat() )
        canvas?.drawRect(rectF, paint)

        //Drawing the circles

        //Dividing the screen height by three
        val heightDividedByThree = height.toFloat() / 3

        //Centering the circle in every one of the three positions
        val circlePostion = arrayOf(
            heightDividedByThree/2,
            heightDividedByThree/2 + heightDividedByThree,
            heightDividedByThree/2 + heightDividedByThree*2)

        // setting the height of circles to stay into 1/3 of parent height
        val circleRadius = heightDividedByThree/2

        paint.style = Paint.Style.FILL

        paint.color = lightOffSemaphoreColor
        when (lightOn)
        {
            Lights.GREEN ->
            {
                canvas?.drawCircle(width.toFloat()/2, circlePostion[0], circleRadius, paint)
                canvas?.drawCircle(width.toFloat()/2, circlePostion[1], circleRadius, paint)

                paint.color = semaphoreGreen
                canvas?.drawCircle(width.toFloat()/2, circlePostion[2], circleRadius, paint)
            }

            Lights.YELLOW ->
            {
                canvas?.drawCircle(width.toFloat()/2, circlePostion[0], circleRadius, paint)
                canvas?.drawCircle(width.toFloat()/2, circlePostion[2], circleRadius, paint)

                paint.color = semaphoreYellow
                canvas?.drawCircle(width.toFloat()/2, circlePostion[1], circleRadius, paint)
            }

            Lights.RED ->
            {
                canvas?.drawCircle(width.toFloat()/2, circlePostion[1], circleRadius, paint)
                canvas?.drawCircle(width.toFloat()/2, circlePostion[2], circleRadius, paint)

                paint.color = semaphoreRed
                canvas?.drawCircle(width.toFloat()/2, circlePostion[0], circleRadius, paint)
            }
        }

        //drawing borders
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = strokeWidth
        paint.color = borderColor

        canvas?.drawCircle(width.toFloat()/2, circlePostion[0], circleRadius, paint)
        canvas?.drawCircle(width.toFloat()/2, circlePostion[1], circleRadius, paint)
        canvas?.drawCircle(width.toFloat()/2, circlePostion[2], circleRadius, paint)

    }

}