package com.example.es03_prof

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        semaphore1.lightOn = SemaphoreView.Lights.YELLOW

        semaphore2.lightOn = SemaphoreView.Lights.RED

        semaphore3.lightOn = SemaphoreView.Lights.GREEN
    }
}
