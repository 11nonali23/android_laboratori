package com.example.cardview_swipable

import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter


class ExampleAdapter() : Adapter<ExampleAdapter.ExampleViewHolder>()
{
    private lateinit var exampleList: ArrayList<ExampleItem>
    private lateinit var itemTouchHelper: ItemTouchHelper

    constructor(exampleList: ArrayList<ExampleItem>): this()
    {
        this.exampleList = exampleList
    }

    inner class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnTouchListener,GestureDetector.OnGestureListener
    {
        var imageView: ImageView
        var textView: TextView
        var mGestureDetector: GestureDetector? = null

        init
        {
            this.imageView = itemView.findViewById(R.id.image)
            this.textView = itemView.findViewById(R.id.text)
            this.mGestureDetector = GestureDetector(itemView.context, this)
            itemView.setOnTouchListener(this)

        }

        override fun onShowPress(e: MotionEvent?) {

        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            return true
        }

        override fun onDown(e: MotionEvent?): Boolean {
            return false
        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            return false
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            return true
        }

        override fun onLongPress(e: MotionEvent?) {
            itemTouchHelper.startDrag(this)
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            mGestureDetector?.onTouchEvent(event)
            return true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.example_item,parent,false)
        return ExampleViewHolder(v)
    }

    override fun getItemCount(): Int
    {
        return exampleList.size
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int)
    {
        var curr = exampleList.get(position)

        holder.imageView.setImageResource(curr.imageResource)
        holder.textView.setText(curr.text)
    }


    fun setTouchHelper(itemTouchHelper: ItemTouchHelper)
    {
        this.itemTouchHelper = itemTouchHelper
    }

    fun onItemMove(fromPosition: Int, toPosition: Int)
    {
        val fromNote: ExampleItem = exampleList.get(fromPosition)
        exampleList.remove(fromNote)
        exampleList.add(toPosition, fromNote)
        notifyItemMoved(fromPosition, toPosition)
    }
}