package com.example.cardview_swipable

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Adapter
import android.widget.Button
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.android.material.floatingactionbutton.FloatingActionButton

import kotlinx.android.synthetic.main.activity_main.*
import java.text.FieldPosition

class MainActivity : AppCompatActivity() {

    private val showingList = ArrayList<ExampleItem>()
    private val exampleItems = ArrayList<ExampleItem>()
    private val exampleItems2 =  ArrayList<ExampleItem>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ExampleAdapter
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var button_ins: FloatingActionButton
    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var itemHelperOnSwipe: ItemTouchHelper.SimpleCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buildReciclerView()
        createExampleList()
        itemHelperOnSwipe = ItemHelperOnSwipe(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, exampleItems, adapter)
        itemTouchHelper = ItemTouchHelper(itemHelperOnSwipe)
        adapter.setTouchHelper(itemTouchHelper)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        button_ins = insert_btn

        button_ins.setOnClickListener {v ->
            ins_item()
        }

        change_btn.setOnClickListener {v ->
            changeAdapterList()
        }

    }

    private fun changeAdapterList()
    {
        showingList.clear()
        showingList.addAll(exampleItems2)

        adapter.notifyDataSetChanged()
    }

    private fun ins_item()
    {
        val toAdd = ExampleItem(R.drawable.ic_android,"Line${exampleItems.size + 1}")
        exampleItems.add(toAdd)
        exampleItems2.add(toAdd)
        adapter.notifyDataSetChanged()
    }

    private fun buildReciclerView()
    {
        exampleItems.add(ExampleItem(R.drawable.ic_android, "Line 1"))
        exampleItems.add(ExampleItem(R.drawable.ic_android, "Line 2"))
        exampleItems.add(ExampleItem(R.drawable.ic_android, "Line 3"))
        exampleItems.add(ExampleItem(R.drawable.ic_android, "Line 4"))

        showingList.addAll(exampleItems)

        exampleItems2.add(ExampleItem(R.drawable.ic_android, "Line 5"))
        exampleItems2.add(ExampleItem(R.drawable.ic_android, "Line 6"))
        exampleItems2.add(ExampleItem(R.drawable.ic_android, "Line 7"))
        exampleItems2.add(ExampleItem(R.drawable.ic_android, "Line 8"))
    }

    private fun createExampleList()
    {
        recyclerView = recycle
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        adapter = ExampleAdapter(showingList)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

}
