package com.example.event_handling_test

import android.view.View

class Controller(model: Model, presenter: Presenter): View.OnClickListener{
    private var model: Model = model
    private var presenter: Presenter = presenter

    override fun onClick(v: View?) {

        this.model.update_model(v)

        this.presenter.update_counter_text("${this.model.counter}")
    }
}