package com.example.event_handling_test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class Presenter : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        Log.d("MainActivity", "creating object")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Creating Model of data
        var data_model = Model(0, "")

        //Creating event handler
        var event_handler = Controller(data_model, this)

        //event handler registration
        plus.setOnClickListener(event_handler)
        minus.setOnClickListener(event_handler)
        reset.setOnClickListener(event_handler)
    }

    fun update_counter_text(text: String){
        counter_view.text = text
    }

}
