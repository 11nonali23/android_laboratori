package com.example.event_handling_test

import android.view.View

class Model(counter:Int, clickOn:String) {
    var counter:Int = counter
    var clickOn:String = clickOn

    fun update_model(v: View?){

        val id = v?.id

        when (id) {

            R.id.plus -> {
                clickOn = "+"; counter++
            }

            R.id.minus -> {
                clickOn = "-"; counter--
            }

            R.id.reset -> {
                clickOn = "RESET"; counter = 0
            }

            else -> print("No id match")

        }
    }
}