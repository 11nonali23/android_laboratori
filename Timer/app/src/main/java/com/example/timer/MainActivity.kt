package com.example.timer

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.LinkedList

//TODO when in real activity upate the color of the rest or workout based of what is running
// in the real project data will be get from BUNDLE EXTRA on itent call

class MainActivity : AppCompatActivity() {
    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds = 0L
    private var timerState = TimerState.Stopped
    private var secondsRemaining = 0L

    private var timerItemList = LinkedList<TimerItem>()
    private var isWorkout = true
    private var isTimerListStarted = false
    private var currTimerItem: TimerItem

    init {
        timerItemList.add(TimerItem(8, 5))
        timerItemList.add(TimerItem(10, 7))
        timerItemList.add(TimerItem(12, 9))
        timerItemList.add(TimerItem(15, 12))
        timerItemList.add(TimerItem(22, 11))
        timerItemList.add(TimerItem(31, 10))

        currTimerItem = timerItemList.poll()
    }

    enum class TimerState {
        Stopped, Paused, Running
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //TIMER LISTENERS-----------------------------------------------------------------------------------------------------------------------------------------------------------
        start.setOnClickListener {

            if (isTimerListStarted) {
                startNewTimer(); updateUIButtons()
            } else {
                startTimersFromList(); updateUIButtons()
            }

            timerState = TimerState.Running
        }

        pause.setOnClickListener { v ->
            timer.cancel()
            timerState = TimerState.Paused
            updateUIButtons()
        }

        reset.setOnClickListener { v ->
            timer.cancel()
            onTimerEnded()
        }

        //TIMER LISTENERS-----------------------------------------------------------------------------------------------------------------------------------------------------------

        textView_remaining.text = timerItemList.size.toString()
    }

    override fun onResume() {
        super.onResume()

        restartTimer()
    }

    override fun onPause() {
        super.onPause()

        if (timerState == TimerState.Running)
            timer.cancel()
        else if (timerState == TimerState.Paused) {
            //TODO show timer in notification
        }

        //save data of timer in shared preferences
        PrefUtilsManager.setPreviousTimerLengthSeconds(this, timerLengthSeconds)
        PrefUtilsManager.setSecondsRemaining(this, secondsRemaining)
        PrefUtilsManager.setTimerState(this, timerState)
        PrefUtilsManager.setTimerItemsList(this, timerItemList)
        PrefUtilsManager.setIsTimerListStarted(this, isTimerListStarted)
        PrefUtilsManager.setIsWorkout(this, isWorkout)
    }


    override fun onDestroy() {
        super.onDestroy()
        PrefUtilsManager.clearAll(this)
    }

    private fun restartTimer() {
        timerState = PrefUtilsManager.getTimerState(this)
        isTimerListStarted = PrefUtilsManager.getIsTimerListStarted(this)
        isWorkout = PrefUtilsManager.getIsWorkout(this)

        if (timerState == TimerState.Stopped)
            setNewTimerAndProgressLength()
        else
            setPreviousTimerLength()

        secondsRemaining =
            if (timerState == TimerState.Running || timerState == TimerState.Paused)
                PrefUtilsManager.getSecondsRemaining(this)
            else
                timerLengthSeconds

        if (timerState == TimerState.Running)
            startNewTimer()

        updateUIButtons()
        //set timer text
        updateTimerUIOnlyText()
    }

    //on finish of the timer. Manage the new Timer call
    private fun onTimerEnded() {
        timerState = TimerState.Stopped

        //changing is workout every time i restart timer
        isWorkout = !isWorkout

        //set the length of the timer to be the one set in SettingsActivity
        //if the length was changed when the timer was running
        setNewTimerAndProgressLength()
        //set the remaining text
        textView_remaining.text = timerItemList.size.toString()

        if (!isWorkout)
            currTimerItem = timerItemList.poll()

        PrefUtilsManager.setSecondsRemaining(this, timerLengthSeconds)
        secondsRemaining = timerLengthSeconds

        startNewTimer()
    }

    private fun startTimersFromList() {
        isTimerListStarted = true
        startNewTimer()
    }

    private fun startNewTimer() {
        //If the timer is started the progress bars will be 0 so i have to reset the current running one
        styleBeforeStart()

        timerState = TimerState.Running
        updateUIButtons()
        updateTimerUI()

        timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() = onTimerEnded()

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateTimerUI()
            }
        }.start()
    }

    private fun styleBeforeStart() {
        //setting progress bar
        if (isWorkout)
            if (progress_circular.progress == progress_circular.max)
                progress_circular.progress = 0
            else
                if (progress_circular_rest.progress == progress_circular_rest.max)
                    progress_circular_rest.progress = 0

        //setting color
        if (isWorkout) {
            workout.setTextColor(Color.parseColor("#E53935"))
            rest.setTextColor(Color.BLACK)
        }
        else{
            rest.setTextColor(Color.parseColor("#E53935"))
            workout.setTextColor(Color.BLACK)
        }
    }


    private fun setNewTimerAndProgressLength()
    {
        val lenghtInMinutes = PrefUtilsManager.getTimerLength(this)

        if (isWorkout)
        {
            timerLengthSeconds = (lenghtInMinutes * currTimerItem.workoutSeconds)
            progress_circular.max = timerLengthSeconds.toInt()
            progress_circular.progress = progress_circular.max

            progress_circular_rest.max = timerLengthSeconds.toInt()
            progress_circular_rest.progress = currTimerItem.restSeconds.toInt()
        }
        else
        {
            timerLengthSeconds = (lenghtInMinutes * currTimerItem.restSeconds)
            progress_circular_rest.max = timerLengthSeconds.toInt()
        }
    }

    private fun setPreviousTimerLength()
    {
        Log.e("TIMER", "prev")
        timerLengthSeconds = PrefUtilsManager.getPreviousTimerLenghtSeconds(this)
        progress_circular.max = timerLengthSeconds.toInt()
    }

    private fun updateTimerUIOnlyText()
    {
        val minutesUntilFinished = secondsRemaining / 60
        val secondsInMinuteUntilFinished = secondsRemaining - minutesUntilFinished * 60
        val secondsStr = secondsInMinuteUntilFinished.toString()
        textView.text = "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0" + secondsStr}"
    }

    private fun updateTimerUI()
    {
        updateTimerUIOnlyText()

        if (isWorkout)
            progress_circular.progress = (timerLengthSeconds - secondsRemaining).toInt()
        else
            progress_circular_rest.progress = (timerLengthSeconds - secondsRemaining).toInt()
    }

    private fun updateUIButtons()
    {
        when(timerState)
        {
            TimerState.Running ->
                { start.isEnabled = false; pause.isEnabled = true; reset.isEnabled = false }

            TimerState.Paused ->
                {start.isEnabled = true; pause.isEnabled = false; reset.isEnabled = true}

            TimerState.Stopped ->
                {start.isEnabled = true; pause.isEnabled = false; reset.isEnabled = false}
        }
    }
}
