package com.example.advanced_chrono2.behaviours

import android.graphics.Color
import android.graphics.Rect
import android.util.Log
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.advanced_chrono2.CustomCardView
import com.example.advanced_chrono2.adapters.SwipableItemsAdapter
import com.example.advanced_chrono2.model.TimerItemData
import com.example.advanced_chrono2.R

class ItemMovementHelper
    (
    dragDirs: Int,
    swipeDirs: Int,
    var list: ArrayList<TimerItemData>,
    val adapter: SwipableItemsAdapter

) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs)

{

    override fun isLongPressDragEnabled(): Boolean
    {
        return false
    }

    override fun isItemViewSwipeEnabled(): Boolean
    {
        return true
    }


    //https://stackoverflow.com/questions/39335214/rounded-corners-cardview-dont-work-in-recyclerview-android/44064045#44064045
    //CHANGING TEH BACKGROUND COLOR MAKES THE CARDVIEW VANISH THE RADIUS PROPERTY SO I NEED TO IMPLEMENT MY OWN CARDVIEW

    //setting the new color in clear view as the same of the first time
    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder)
    {
        viewHolder.itemView.setBackgroundColor((Color.parseColor("#FFFFFF")))
    }

    //Setting the movement color if user is dragging
    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int)
    {
        super.onSelectedChanged(viewHolder, actionState)

        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG)
        {
            viewHolder?.itemView?.setBackgroundColor(Color.parseColor("#42000000"))
        }
    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int
    {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(
            dragFlags,
            swipeFlags
        )
    }

    //Behaviour on movement of the item
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean
    {
        adapter.onItemMove(viewHolder.layoutPosition, target.layoutPosition)
        return true
    }

    //Behaviour on swipe element
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int)
    {
        list.removeAt(viewHolder.layoutPosition)
        adapter.notifyDataSetChanged()
    }
}