package com.example.advanced_chrono2.adapters


import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.advanced_chrono2.model.TimerItemData
import com.example.advanced_chrono2.R

//This adapter is used to manage the items of the activities taht can be swiped to be deleted and also moved around them

class SwipableItemsAdapter() : Adapter<SwipableItemsAdapter.SwipableItemsViewHolder>()
{
    private lateinit var exampleList: ArrayList<TimerItemData>
    private lateinit var itemTouchHelper: ItemTouchHelper

    constructor(exampleList: ArrayList<TimerItemData>): this()
    {
        this.exampleList = exampleList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SwipableItemsViewHolder
    {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.timer_item_layout ,parent,false)
        return SwipableItemsViewHolder(v)
    }

    override fun getItemCount(): Int
    {
        return exampleList.size
    }

    override fun onBindViewHolder(holder: SwipableItemsViewHolder, position: Int)
    {
        var curr = exampleList.get(position)

        holder.imageView.setImageResource(curr.imageResource)

        holder.textView.setText(curr.name)
    }


    fun setTouchHelper(itemTouchHelper: ItemTouchHelper)
    {
        this.itemTouchHelper = itemTouchHelper
    }

    fun onItemMove(fromPosition: Int, toPosition: Int)
    {
        val fromNote: TimerItemData = exampleList.get(fromPosition)
        exampleList.remove(fromNote)
        exampleList.add(toPosition, fromNote)
        notifyItemMoved(fromPosition, toPosition)
    }


    inner class SwipableItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnTouchListener,GestureDetector.OnGestureListener
    {
        var imageView: ImageView
        var textView: TextView
        var mGestureDetector: GestureDetector? = null

        init
        {
            this.imageView = itemView.findViewById(R.id.timer_item_image)
            this.textView = itemView.findViewById(R.id.timer_item_text)
            this.mGestureDetector = GestureDetector(itemView.context, this)
            itemView.setOnTouchListener(this)

        }

        override fun onShowPress(e: MotionEvent?)
        {
            //do nothing
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean
        {
            return true
        }

        override fun onDown(e: MotionEvent?): Boolean
        {
            return false
        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean
        {
            return false
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean
        {
            return true
        }

        override fun onLongPress(e: MotionEvent?)
        {
            itemTouchHelper.startDrag(this)
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean
        {
            mGestureDetector?.onTouchEvent(event)
            return true
        }
    }
}