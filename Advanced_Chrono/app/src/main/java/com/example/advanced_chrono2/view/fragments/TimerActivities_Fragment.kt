package com.example.advanced_chrono2.view.fragments

import android.app.AlertDialog
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.advanced_chrono2.adapters.ActivityHorizontalAdapter
import com.example.advanced_chrono2.adapters.SwipableItemsAdapter
import com.example.advanced_chrono2.model.TimerItemData
import com.example.advanced_chrono2.behaviours.ItemMovementHelper
import com.example.advanced_chrono2.R
import kotlinx.android.synthetic.main.timer_items_layout.*
import kotlin.collections.ArrayList

//Provvisorio per lista di attività

class TimerActivities_Fragment : Fragment()
{

    companion object
    {
        private const val TAG = "TIMER FRAGMENT CICLE"

    }

    private val exampleItems = ArrayList<TimerItemData>()
    private val exampleItems2 =  ArrayList<TimerItemData>()

    private lateinit var activityItemList: RecyclerView
    private lateinit var activityList: RecyclerView //NEW <=======

    private lateinit var timerItemAdapter: SwipableItemsAdapter
    private lateinit var activityItemAdapter: ActivityHorizontalAdapter

    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var itemHelperOnSwipe: ItemTouchHelper.SimpleCallback

    //This method shows when a new fragment is created
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "FRAGMENT TIMER CREATED")
    }

    //when creating the view inflating the chronometer layout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.timer_items_layout, container, false)
    }

    //here I will init all my components
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        buildReciclerView()

        createItemList()
        createActivityList()



        //TODO make right
        timer_items_button.setOnClickListener {
            //Aggiunta di esempio
            exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 1"))
            timerItemAdapter.notifyDataSetChanged()

            val dialogBuilder = AlertDialog.Builder(this.context, R.style.AlertDialogCustom)
            val dialogView = layoutInflater.inflate(R.layout.add_timer_item_layout, null)
            dialogBuilder.setView(dialogView)

            dialogBuilder.setPositiveButton("YES!") { _, _ ->
                Toast.makeText(this.context, "OK", Toast.LENGTH_LONG).show()
            }
            dialogBuilder.show()
        }
    }

    //FUNZIONI PROVVISORIE

    private fun buildReciclerView()
    {
        /*exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 1"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 2"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 3"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 1"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 2"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 3"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 1"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 2"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 3"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 1"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 2"))
        exampleItems.add(TimerItemData(R.drawable.ic_timer_black, "Timer 3"))*/


        exampleItems2.add(TimerItemData(R.drawable.ic_timer_black,"Attività 1"))
        exampleItems2.add(TimerItemData(R.drawable.ic_timer_black,"Attività 2"))
        exampleItems2.add(TimerItemData(R.drawable.ic_timer_black,"Attività 3"))
        exampleItems2.add(TimerItemData(R.drawable.ic_timer_black,"Attività 4"))
        exampleItems2.add(TimerItemData(R.drawable.ic_timer_black,"Attività 5"))

    }

    private fun createItemList()
    {
        activityItemList = activity_item_recycler

        activityItemList.setHasFixedSize(true)

        timerItemAdapter = SwipableItemsAdapter(exampleItems)

        activityItemList.layoutManager = LinearLayoutManager(this.context)
        activityItemList.adapter = timerItemAdapter

        //NEW
        activityItemList.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect.set(20, 0,20,0)
            }
        })

        setActivityListMoventBehaviour()
    }

    private fun setActivityListMoventBehaviour()
    {
        //itemMovementHelper defines behaviour of recyclerView on user inputs
        itemHelperOnSwipe =
            ItemMovementHelper(
                0,
                ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
                exampleItems,
                timerItemAdapter
            )

        itemTouchHelper = ItemTouchHelper(itemHelperOnSwipe)

        timerItemAdapter.setTouchHelper(itemTouchHelper)

        itemTouchHelper.attachToRecyclerView(activityItemList)
    }

    private fun createActivityList()
    {
        activityList = activity_recycle

        activityList.setHasFixedSize(true)

        activityList.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)

        activityItemAdapter = ActivityHorizontalAdapter(exampleItems2)
        activityList.adapter = activityItemAdapter

        //setting a dynamic center for horizontal activity
        activityList.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val cardHeight = 200
                val containerHeight = resources.getDimensionPixelOffset(R.dimen.max_container_height)
                var topBottomPadding = (containerHeight/2 - cardHeight)/2
                outRect.set(10, topBottomPadding,10,topBottomPadding)
            }
        })
    }
}
