package com.example.advanced_chrono2.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.advanced_chrono2.view.fragments.Data_Fragment
import com.example.advanced_chrono2.view.fragments.TimerActivities_Fragment
import com.example.advanced_chrono2.view.fragments.Home_Chronometer_Fragment


private const val pages = 3

//implement better this in order to not create every time a fragment instance
class ViewPagerAdapter(fa: FragmentActivity)  : FragmentStateAdapter(fa)
{
    //I want to use the adapter to know wether there are some new data to plot or not
    companion object
    {
        private var areNewUpdatesAvailable = false
    }

    override fun getItemCount(): Int
    {
        return pages
    }

    override fun createFragment(position: Int): Fragment
    {
        when (position)
        {
            0 -> return Home_Chronometer_Fragment()
            1 -> return TimerActivities_Fragment()
            2 -> return Data_Fragment()
            else -> return Home_Chronometer_Fragment()
        }
    }
}