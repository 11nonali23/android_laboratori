package com.example.todo_app

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.util.*
import kotlin.collections.ArrayList

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){
    //This companionnì object defines all the constant and static values
    companion object
    {
        val TAG = "DatabaseHelper"
        //If change the databaseschema, version must be incremented
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "todolist.db"
        //Books table name
        val TABLE_NAME = "todoItems"
        //Books table Columns names
        val KEY_ID = "id"
        val KEY_TASK = "task"
        val KEY_DATE = "creation_date"

        //better no use auotincrement if not needed
        private val SQL_CREATE_ENTRIES: String =
            "CREATE TABLE $TABLE_NAME (" +
                    "$KEY_ID integer primary key autoincrement, " +
                    "$KEY_TASK TEXT not null, " +
                    "$KEY_DATE INTEGER" +
                    ");"

        private val SQL_SELECT_ALL = "SELECT * FROM $TABLE_NAME"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_NAME"
    }

    //Executed only when created. I will create the entries
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_ENTRIES)
        Log.d(TAG, "onCreate(): create tables")
    }

    //If i upgrade i will drop tables and restart the db with on create
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    //If i downgrade i will do onUpgrade passing oldVersion
    override fun onDowngrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }


    fun getAllItems(): Collection<ToDoItem>?
    {
        var items: ArrayList<ToDoItem> = ArrayList()

        val db: SQLiteDatabase = this.writableDatabase

        val cursor: Cursor = db.rawQuery(SQL_SELECT_ALL, null)
        if(cursor.moveToFirst())
        {
            do
            {
              var task = ToDoItem()
                task.id = cursor.getString(0).toLong()
                task.todo = cursor.getString(1)
                val createOn = GregorianCalendar()
                createOn.timeInMillis = cursor.getLong(2)
                task.createOn = createOn
                items.add(0, task)

            } while (cursor.moveToNext())
        }
        db.close()
        Log.d(TAG, "getAllItems(): $items")
        return items
    }

    fun deleteItem(item: ToDoItem)
    {
        val db: SQLiteDatabase = this.writableDatabase

        db.delete(
            TABLE_NAME,
            "$KEY_ID = ?",
            arrayOf(item.id.toString())
        )
        db.close()
        Log.d(TAG, "deleted item $item")
    }

    fun deleteAllItems()
    {
        val db: SQLiteDatabase = this.writableDatabase

        db.execSQL("delete from $TABLE_NAME")


        Log.d(TAG, "deleted all items")
    }

    fun insertItem(item: ToDoItem): Long
    {
        val db: SQLiteDatabase = this.writableDatabase

        //Content Values is a dictonary with key = column val = value of column
        val values = ContentValues()
        values.put(KEY_TASK, item.todo)
        values.put(KEY_DATE, item.createOn.timeInMillis)
        item.id = db.insert(TABLE_NAME, null, values)

        db.close()

        Log.d(TAG, "added item $item")
        return item.id
    }


}

