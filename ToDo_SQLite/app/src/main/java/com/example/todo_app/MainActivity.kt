package com.example.todo_app

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates


//In this application I will try to add ToDoItems into database in order to save the data

class MainActivity : AppCompatActivity() {
    var toDoItems = ArrayList<ToDoItem>()
    var adapter: ArrayAdapter<ToDoItem> by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helper = DatabaseHelper(this)
        toDoItems.addAll(helper.getAllItems()!!)

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, toDoItems)

        list_item.adapter = adapter

        list_item.setOnItemLongClickListener() {parent, view, position, id ->
            onLongClick(position)
            true
        }

    }

    //When user add an item it will go the ListView or Toast error if empty
    fun addItem (view: View)
    {
        val ToDo = ToDo_view.text.toString()
        if(ToDo.length == 0)
        {
            Toast.makeText(applicationContext, "Empty ToDo value", Toast.LENGTH_LONG).show()
            return
        }

        var newTodo = ToDoItem(ToDo)
        //insert element into db
        val helper = DatabaseHelper(this)
        helper.insertItem(newTodo)
        //insert element into arraylist and notifying adapter
        toDoItems.add(newTodo)
        adapter.notifyDataSetChanged()

        //clear text field
        ToDo_view.setText("")

        //Hide the soft key
        var inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(ToDo_view.applicationWindowToken, 0)

        Log.d("Main Activity", "Added Todo to database")
    }

    //deleting item on long click
    fun onLongClick(position: Int)
    {
        val item = list_item.getItemAtPosition(position) as ToDoItem
        //delete item from db
        val helper = DatabaseHelper(this)
        helper.deleteItem(item)
        //delete item from the listview data structure and notify to adapter
        toDoItems.removeAt(position)
        adapter.notifyDataSetChanged()

        Log.d("Main activity", "Deleted item")
    }

    //It will eliminate all elements
    fun empty(view: View)
    {
        //deleting from db all items
        val helper = DatabaseHelper(this)
        helper.deleteAllItems()
        toDoItems.clear()
        adapter.notifyDataSetChanged()
    }
}

