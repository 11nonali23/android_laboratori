package com.example.todo_app

import java.text.SimpleDateFormat
import java.util.*

//Class to model a to do item

class ToDoItem() {
    constructor(msg: String): this()
    {
        todo = msg
    }

    var id: Long = -1
    var todo: String = ""
    var createOn: GregorianCalendar = GregorianCalendar()

    override fun toString(): String
    {
        var currentDate = SimpleDateFormat("dd/MM/yyyy",
            Locale.ITALIAN).format(createOn.getTime());

        return "$currentDate\n  $todo"
    }
}