package com.example.laboratorio_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/*In questa applicazione proverò a:
    1 => RENDERE L' APPLICAZIONE MULTILINGA INSERENDO UNA NUOVA RISORSA:
        per fare ciò
            creo in res una cartella res/values-it
            clicco su strings.xml e apro l'editor
            aggiungo l'italiano
            creo una stringa nei miei componenti GUI e faccio alt + enter e extract resource

    2 =>
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
