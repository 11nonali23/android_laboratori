package com.example.mpcharttutorial

import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val values: ArrayList<Entry> = ArrayList()
    private lateinit var mChart: LineChart

    private lateinit var set1: LineDataSet

    private var x = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mChart = chart
        mChart.setTouchEnabled(true)
        mChart.setPinchZoom(true)

        values.add(Entry(x++, 50F))
        values.add(Entry(x++, 40F))
        values.add(Entry(x++, 130F))
        values.add(Entry(x++, 10F))
        values.add(Entry(x++, 140F))
        values.add(Entry(x++, 150F))
        values.add(Entry(x++, 80F))
        values.add(Entry(x++, 100F))
        values.add(Entry(x++, 50F))
        values.add(Entry(x++, 40F))
        values.add(Entry(x++, 80F))
        values.add(Entry(x++, 100F))
        values.add(Entry(x++, 50F))
        values.add(Entry(x++, 40F))
        values.add(Entry(x++, 50F))
        values.add(Entry(x++, 40F))
        values.add(Entry(x++, 80F))
        values.add(Entry(x++, 100F))
        values.add(Entry(x++, 50F))
        values.add(Entry(x++, 40F))

        drawGraph()

        btn.setOnClickListener {
            Log.e("click", "click")
            set1.addEntry(Entry(x++,(x + 100F)))
            mChart.data.notifyDataChanged()
            mChart.notifyDataSetChanged()
            mChart.invalidate()
        }
    }

    private fun drawGraph()
    {
        set1 = LineDataSet(values, "Sample Data")
        set1.setDrawIcons(false)
        set1.enableDashedLine(10f, 5f, 0f)
        set1.enableDashedHighlightLine(10f, 5f, 0f)
        set1.color = Color.DKGRAY
        set1.setCircleColor(Color.DKGRAY)
        set1.lineWidth = 1f
        set1.circleRadius = 3f
        set1.setDrawCircleHole(false)
        set1.valueTextSize = 9f
        set1.setDrawFilled(true)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        if (Utils.getSDKInt() >= 18)
            set1.fillColor = Color.CYAN
        else
            set1.fillColor = Color.CYAN

        val dataSets: ArrayList<ILineDataSet> = ArrayList()
        dataSets.add(set1)
        val data = LineData(dataSets)
        mChart.data = data
        mChart.isHighlightPerDragEnabled = true
    }

}
