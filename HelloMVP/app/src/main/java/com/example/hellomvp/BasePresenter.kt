package com.example.hellomvp

//ATTENZIONE if the activity gets killed and recreated the fragments inside the view pager are restored but the presenters not.

//usare un presenter su un fragment non è una buona pratica

/*
Presenters are unaware of “how” to present data to the user: this is a view task
All they have is a View instance that takes this data as input and performs the presentation tasks.
So if you were to display an error to the user in case he forgets to fill a field in the form,
all your presenter has to do is call the view.displayError(errorMessage) method on the View instance.

Now realize that the Presenter doesn’t know what happens next,
whether the error is displayed as a Toast, or as a Dialog or a Snackbar or what color is the message etc.
 */

/*
Think of the presenters as the Boss here.
 Similar to how you used to manage everything from your activity in the I Love MainActivity era,
 Presenters now handles all the management tasks and keeps your activities/fragments less busy.
 So your DatabaseHandler, ApiHandler, PeanutButterMaker and instances of whatnot
 you just provide it with the Presenter instance and place all the other important stuff inside your Presenter.
 */


interface BasePresenter
{
    fun performAddition(numberOne: String?, numberTwo: String?)
    fun isEmptyInput(numOne: String?, numTwo: String?): Boolean
}