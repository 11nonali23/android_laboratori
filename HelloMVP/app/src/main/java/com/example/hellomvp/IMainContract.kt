package com.example.hellomvp

interface IMainContract
{
    interface MainPresenter : BasePresenter

    interface MainView : BaseView

}