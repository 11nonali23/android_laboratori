package com.example.hellomvp

import android.R
import android.content.Context


class MainBasePresenter(val  mView: BaseView, val context: Context) : IMainContract.MainPresenter {

    /*??
    @Override
    public void start() {
        // Do your initialization work here
    }
     */

    override fun performAddition(numberOne: String?, numberTwo: String?) {
        if (isEmptyInput(numberOne, numberTwo))
        {
            // Display error message if any of the inputs is empty
            mView.displayMessage(context.getString(R.string.no))

        } else {
            // Compute and pass the result to view for display
            val firstNumber = numberOne?.toInt()
            val secondNumber = numberTwo?.toInt()
            val result = secondNumber?.let { firstNumber?.plus(it) }
            if (result != null) {
                mView.displayResult(result)
            }
        }
    }

    override fun isEmptyInput(numOne: String?, numTwo: String?): Boolean {
        return numOne == null || numOne.length == 0 || numTwo == null || numTwo.length == 0
    }
}