package com.example.hellomvp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.act_main.*


//dev.to/xuhaibahmad/going-clean-with-mvp-a-guide-to-model-view-presenter-on-android-1pik

/*In order to make code testable
The Controllers should be separate classes that don’t extend or use any Android class, and same for the Models.*/

/*Model is an interface responsible for managing data.
Model’s responsibilities include using APIs, caching data, managing databases and so on.
The model can also be an interface that communicates with other modules in charge of these responsibilities.
 */



class MainActivity : AppCompatActivity(), IMainContract.MainView {

    private var mPresenter: BasePresenter? = MainBasePresenter(this, this)
        get() {return mPresenter}
        set(value){field = value}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)

        mButton.setOnClickListener {
            onAddButtonClicked()
        }
    }

    override fun displayMessage(text: String?) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun displayResult(result: Int) {
        resultText.setText("$result")
    }

    fun onAddButtonClicked() {
        val numberOne: String = mNumberOneEditText.getText().toString()
        val numberTwo: String = mNumberTwoEditText.getText().toString()
        mPresenter?.performAddition(numberOne, numberTwo)
    }


}
