package com.example.hellomvp

/*
Do not confuse this with your XML views.
Views in MVP are simply the components responsible for managing the user interface.
This includes all the UI manipulation operations such as
    setting text to some TextView,
    toggling visibility of Views,
    displaying animations etc.
Basically, everything that has to do with the User Interface, goes into this place.
 */


interface BaseView
{
    fun displayMessage(text: String?)
    fun displayResult(result: Int)
}